﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleAttractor : MonoBehaviour
{
    [SerializeField]
    private float speed = 5f;

    ParticleSystem system;
    private void Start()
    {
        system = GetComponent<ParticleSystem>();
        system.Play();
        StartCoroutine(Effect());
    }
    public IEnumerator Effect()
    {
        yield return new WaitForSeconds(0.25f);
        float startTime = Time.time;
        while (Time.time < startTime + 10f)
        {
            ParticleSystem.Particle[] starparticles = new ParticleSystem.Particle[system.particleCount];
            system.GetParticles(starparticles, starparticles.Length);

            for (int i = 0; i < starparticles.Length; i++)
            {
                ParticleSystem.Particle current = starparticles[i];
                Vector3 currentPlayerPosition = transform.InverseTransformPoint(Player.currentPlayer.position);
                float distance = Vector3.Distance(current.position, currentPlayerPosition);
                current.position = Vector3.MoveTowards(current.position, currentPlayerPosition, (speed / distance) * Time.deltaTime);
                if (distance < 0.1f)
                    current.remainingLifetime = 0f;
                starparticles[i] = current;

            }
            system.SetParticles(starparticles, starparticles.Length);
            yield return null;
        }
        Destroy(gameObject);
    }
    public void Update()
    {
        
        
    }
    
}
