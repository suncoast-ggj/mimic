﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuController : MonoBehaviour
{
    public GameObject mainUI;

    public GameObject creditsUI;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void LoadMainUI()
    {
        mainUI.SetActive(true);
        creditsUI.SetActive(false);
    }

    public void LoadCredits()
    {
        mainUI.SetActive(false);
        creditsUI.SetActive(true);
    }

    public void LoadGame()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(1);
    }
}
