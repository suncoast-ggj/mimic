﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnPlayerSelectedEvent : GameEvent
{
    public Player selectedPlayer { get; private set; }
    public OnPlayerSelectedEvent(Player newPlayer)
    {
        selectedPlayer = newPlayer;
    }
}
