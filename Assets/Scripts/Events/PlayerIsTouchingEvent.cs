﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerIsTouchingEvent : GameEvent
{
    public List<Player> collisionPlayers { get; private set; }

    public PlayerIsTouchingEvent(List<Player> collisionPlayer)
    {
        this.collisionPlayers = collisionPlayer;
    }
}
