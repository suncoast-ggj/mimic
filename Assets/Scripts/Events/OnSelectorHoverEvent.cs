﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnSelectorHoverEvent : GameEvent
{
    public Player hoveredPlayer { get; private set; }
    public OnSelectorHoverEvent(Player hoveredPlayer)
    {
        this.hoveredPlayer = hoveredPlayer;
    }
}
