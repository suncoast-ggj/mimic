﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnCameraControllerStateChange : GameEvent
{

    public CameraController.States newState { get; private set; }
    public OnCameraControllerStateChange(CameraController.States newState)
    {
        this.newState = newState;
    }
}
