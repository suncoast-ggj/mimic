﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Player))]
public class CollisionDetector : MonoBehaviour
{
    private List<Player> _touchingPlayerObjects = new List<Player>();

    public bool isTouchingOtherPlayerObjects { get { return _touchingPlayerObjects.Count > 0; } }

    public Player[] touchingPlayerObjects { get { return _touchingPlayerObjects.ToArray(); } }

    private Player owningPlayer;

    [SerializeField]
    private AudioSource bang;

    private void Start()
    {
        owningPlayer = GetComponent<Player>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        Player otherPlayer = collision.gameObject.GetComponent<Player>();
        if (otherPlayer != null && !_touchingPlayerObjects.Contains(otherPlayer))
        {
            _touchingPlayerObjects.Add(otherPlayer);
        }
        
        if (bang != null)
        {
            bang.volume = Mathf.Clamp(collision.impulse.magnitude/4, 0f, 1f);
            bang.Play();
        }
    }



    private void OnCollisionExit(Collision collision)
    {
        Player otherPlayer = collision.gameObject.GetComponent<Player>();
        if (otherPlayer != null)
            _touchingPlayerObjects.Remove(otherPlayer);
    }

}
