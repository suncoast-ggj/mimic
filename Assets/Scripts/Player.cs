﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody), typeof(Highlighter), typeof(CollisionDetector)), 
    RequireComponent(typeof(GroundSensor))]
public class Player : MonoBehaviour
{
    /// <summary>
    /// Which object is the player currently possessing?
    /// </summary>
    public static Player currentPlayer
    {
        get; private set;
    }

    /// <summary>
    /// The position of the player
    /// </summary>
    public Vector3 position
    {
        get { return transform.position; }
    }

    /// <summary>
    /// The player's rigidbody
    /// </summary>
    private Rigidbody cachedRigidBody;
    
    /// <summary>
    /// Scaling factor applied to movement force
    /// </summary>
    private float forceMultiplier = 15f;

    [SerializeField]
    private float forceAdjust = 1f;

    /// <summary>
    /// Mass of the player object
    /// </summary>
    public float mass { get { return cachedRigidBody.mass; } }

    /// <summary>
    /// Clamps the velocity of the player
    /// </summary>
    [SerializeField]
    private float maxVelocity = 3f;
    
    /// <summary>
    /// Force applied to the player in order to jump
    /// </summary>
    private float jumpForce = 200f;

    /// <summary>
    /// Used to detect if I'm touching other playable objects
    /// </summary>
    private CollisionDetector detector;

    /// <summary>
    /// Checks if player is touching the ground
    /// </summary>
    private GroundSensor groundSensor;

    /// <summary>
    /// The collider attached to this Player
    /// </summary>
    private Collider cachedCollider;

    /// <summary>
    /// Is this player touching other players?
    /// </summary>
    public bool isTouchingOtherPlayers { get { return detector.isTouchingOtherPlayerObjects; } }

    /// <summary>
    /// Size factor of this object;
    /// </summary>
    public float size
    {
        get; private set;
    }

    // Start is called before the first frame update
    void Start()
    {
        if (currentPlayer == null)
            Events.instance.Raise(new OnPlayerSelectedEvent(this)); //The first object to be initialized is the first object the player is
        cachedRigidBody = GetComponent<Rigidbody>();
        detector = GetComponent<CollisionDetector>();
        groundSensor = GetComponent<GroundSensor>();
        cachedCollider = GetComponent<Collider>();
        size = Mathf.Max(cachedCollider.bounds.size.x, cachedCollider.bounds.size.y, cachedCollider.bounds.size.z);
        Events.instance.AddListener<OnPlayerSelectedEvent>(OnNewPlayerSelection);
    }

    /// <summary>
    /// pushes the object in the given direction vector
    /// </summary>
    /// <param name="moveVector"></param>
    public void Move(Vector3 moveVector)
    {
        moveVector.Normalize();

        Vector3 force = moveVector * forceMultiplier * mass;
        force = force + (Vector3.up * (force.magnitude / 2f));
        if (!groundSensor.touchingGround)
            force = moveVector * forceMultiplier * mass / 4f; // reducing movement force if I'm in the air

        cachedRigidBody.AddForce(force * forceAdjust);
        //clamping velocity
        if (cachedRigidBody.velocity.magnitude > maxVelocity)
            cachedRigidBody.velocity = cachedRigidBody.velocity.normalized * maxVelocity;
    }

    /// <summary>
    /// Makes the player jump
    /// </summary>
    public void Jump()
    {
        if (groundSensor.touchingGround)
            cachedRigidBody.AddForce(Vector3.up * jumpForce * forceAdjust / (mass/2f));
    }

    // Update is called once per frame
    void Update()
    {
        if (currentPlayer == this)  
        {
            cachedRigidBody.WakeUp();
            Events.instance.Raise(new PlayerIsTouchingEvent(new List<Player>(detector.touchingPlayerObjects)));
        }
    }

    private void OnNewPlayerSelection(OnPlayerSelectedEvent e)
    {
        currentPlayer = e.selectedPlayer;
    }
}
