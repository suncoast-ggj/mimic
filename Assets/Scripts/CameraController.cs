﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ABXY.Stately;

public class CameraController : MonoBehaviour
{
    public Transform transformRoot;
    public Transform rotationRoot;
    public Transform cameraTransform;
    public new Camera camera;
    public GameObject changeEffectPrefab;

    [SerializeField]
    private float xRotationRate = 1f;

    [SerializeField]
    private float yRotationRate = 1f;

    [SerializeField]
    private bool invertY = false;

    private Quaternion startingCameraRotation;
    private float startingCameraFOV = 60f;

    private StateMachine stateMachine = new StateMachine(typeof(States));

    public enum States { REGULAR_MOVEMENT, SELECTION_MOVEMENT}

    private static CameraController instance;

    /// <summary>
    /// list of player objects currently being touched, updates every frame
    /// </summary>
    List<Player> touchingPlayables = new List<Player>();

    public static Vector3 cameraPosition
    {
        get { return instance.cameraTransform.position; }
    }

    public static Vector3 cameraForwardVector
    {
        get { return instance.cameraTransform.TransformDirection(Vector3.forward); }
    }

    [SerializeField]
    private AudioSource grokStartSound;

    [SerializeField]
    private AudioSource grokSound;

    // Start is called before the first frame update
    void Start()
    {
        Cursor.visible = false;
        instance = this;
        startingCameraRotation = cameraTransform.localRotation;
        stateMachine.AddHandler(StatelyEnums.HandlerType.OnActivate, OnRegularMoveStart, States.REGULAR_MOVEMENT);
        stateMachine.AddHandler(StatelyEnums.HandlerType.WhileActivated, DoRegularMove, States.REGULAR_MOVEMENT);

        stateMachine.AddHandler(StatelyEnums.HandlerType.OnDeactivate, OnSelectionMoveEnd, States.SELECTION_MOVEMENT);
        stateMachine.AddHandler(StatelyEnums.HandlerType.OnActivate, OnSelectionMoveStart, States.SELECTION_MOVEMENT);
        stateMachine.AddHandler(StatelyEnums.HandlerType.WhileActivated, DoSelectionMove, States.SELECTION_MOVEMENT);
        stateMachine.SetDefaultState(States.REGULAR_MOVEMENT);
        stateMachine.Start(this);
        Events.instance.AddListener<PlayerIsTouchingEvent>(OnPlayableTouchChange);
    }
    private void FixedUpdate()
    {

        stateMachine.Update();
    }    
    

    private void OnSelectionMoveStart()
    {
        Events.instance.Raise(new OnCameraControllerStateChange(States.SELECTION_MOVEMENT));
        if (grokStartSound != null)
            grokStartSound.Play();
    }

    private void OnSelectionMoveEnd()
    {
        if (grokStartSound != null)
            grokStartSound.Stop();
    }

    private void DoSelectionMove()
    {
        MoveToPlayer();

        // doing camera rotation
        // horizontal
        Vector2 rotationControlVector = CalculateMouseControlVector();
        cameraTransform.Rotate(Vector3.up, rotationControlVector.x * xRotationRate * 300f * Time.deltaTime, Space.World);
        //vertical
        int inversion = invertY ? -1 : 1;
        cameraTransform.Rotate(cameraTransform.TransformDirection(Vector3.right) * inversion,
            rotationControlVector.y * yRotationRate * 100f * Time.deltaTime,
            Space.World);

        // switching modes if needed
        if (!Input.GetKey(KeyCode.LeftShift) || 
            (Player.currentPlayer != null &&
            !Player.currentPlayer.isTouchingOtherPlayers))
        {
            stateMachine.ActivateState(States.REGULAR_MOVEMENT);
        }

        // doing camera zoom
        camera.fieldOfView = Mathf.MoveTowards(camera.fieldOfView, startingCameraFOV, 50f * Time.deltaTime);

        //raycasting for next player 
        Player foundPlayer = DoPlayerRaycast();
        Events.instance.Raise(new OnSelectorHoverEvent(foundPlayer));
        if (foundPlayer != Player.currentPlayer && Input.GetMouseButtonDown(0) && touchingPlayables.Contains(foundPlayer)){
            GameObject.Instantiate(changeEffectPrefab, Player.currentPlayer.position, Quaternion.identity);
            Events.instance.Raise(new OnPlayerSelectedEvent(foundPlayer));
            grokSound.Play();

        }


    }

    private void OnRegularMoveStart()
    {
        Events.instance.Raise(new OnCameraControllerStateChange(States.REGULAR_MOVEMENT));
    }

    private void DoRegularMove()
    {
        //doing player movement
        if (Player.currentPlayer != null)
        {
            MoveToPlayer();
            Player.currentPlayer.Move(CalculateControlVector());
        }

        // Doing Jump
        if (Input.GetKeyDown(KeyCode.Space)){
            Player.currentPlayer.Jump();
        }

        // doing camera rotation
        // horizontal
        Vector2 rotationControlVector = CalculateMouseControlVector();
        transformRoot.Rotate(Vector3.up, rotationControlVector.x *  xRotationRate * 300f * Time.deltaTime);
        //vertical
        int inversion = invertY ? -1 : 1;
        rotationRoot.Rotate(transformRoot.TransformDirection(Vector3.right) * inversion,
            rotationControlVector.y * yRotationRate * 100f * Time.deltaTime,
            Space.World);

        //clamping vertical rotation
        Vector3 currentEulerRotation = rotationRoot.localRotation.eulerAngles;
        float currentXRotation = rotationRoot.localRotation.eulerAngles.x;
        if (currentXRotation > 180)
        {
            currentXRotation = Mathf.Clamp(currentXRotation, 335, 360f);
        }
        else
        {
            currentXRotation = Mathf.Clamp(currentXRotation, 0f, 30);
        }

        rotationRoot.localRotation = Quaternion.Euler(currentXRotation, currentEulerRotation.y, currentEulerRotation.z);

        // doing camera zoom
        camera.fieldOfView = Mathf.MoveTowards(camera.fieldOfView, 
            startingCameraFOV * (Mathf.Log10(Player.currentPlayer.size + 4) + Mathf.Log10(Player.currentPlayer.size + 1)), 
            50f * Time.deltaTime);

        //rotating camera towards neutral position
        Quaternion currentCameraRotation = cameraTransform.localRotation;
        currentCameraRotation = Quaternion.RotateTowards(currentCameraRotation, startingCameraRotation, 300f * Time.deltaTime);
        cameraTransform.localRotation = currentCameraRotation;

        // switching modes if needed
        if (Input.GetKey(KeyCode.LeftShift) &&
            Player.currentPlayer != null &&
            Player.currentPlayer.isTouchingOtherPlayers)
        {
            stateMachine.ActivateState(States.SELECTION_MOVEMENT);
        }
    }

    /// <summary>
    /// calculates the rotation control vector from mouse input
    /// </summary>
    /// <returns></returns>
    private Vector2 CalculateMouseControlVector()
    {
        float horizontal = Input.GetAxis("Mouse X");
        float vertical = Input.GetAxis("Mouse Y");
        Vector2 mouseControlVector = new Vector2(horizontal, vertical);
        return mouseControlVector;
    }

    private void MoveToPlayer()
    {
        if (Player.currentPlayer != null)
        {
            float distanceToTarget = Vector3.Distance(transformRoot.position, Player.currentPlayer.position);
            float speed = distanceToTarget * distanceToTarget * Time.deltaTime * 2f;
            transformRoot.position = Vector3.MoveTowards(transformRoot.position, Player.currentPlayer.position, speed);
        }
    }

    /// <summary>
    /// Calculates the movement control vector from keyboard input
    /// </summary>
    /// <returns></returns>
    private Vector3 CalculateControlVector()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");
        Vector3 localControlVector = new Vector3(horizontal, 0f, vertical);
        return transformRoot.TransformDirection(localControlVector).normalized;
    }

    private Player DoPlayerRaycast()
    {
        RaycastHit[] results = Physics.RaycastAll(cameraPosition, cameraForwardVector);
        foreach(RaycastHit hit in results)
        {
            Player player = hit.collider.gameObject.GetComponent<Player>();
            if (player != null)
                return player;
        }
        return null;
    }

    private void OnPlayableTouchChange(PlayerIsTouchingEvent e)
    {
        touchingPlayables = e.collisionPlayers;
    }
}
