﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundSensor : MonoBehaviour
{
    public bool touchingGround { get; private set; }

    private List<Collider> collidersCurrentlyTouching = new List<Collider>();

    private void Update()
    {
        CheckGround();   
    }

    private void CheckGround()
    {
        RaycastHit[] hits = Physics.RaycastAll(transform.position, Vector3.down);
        foreach (RaycastHit hit in hits)
        {
            if (collidersCurrentlyTouching.Contains(hit.collider))
            {
                touchingGround = true;
                return;
            }
        }
        touchingGround = false;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!collidersCurrentlyTouching.Contains(collision.collider))
        {
            collidersCurrentlyTouching.Add(collision.collider);
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        collidersCurrentlyTouching.Remove(collision.collider);
    }
}
