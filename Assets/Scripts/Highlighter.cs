﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Player), typeof(MeshRenderer))]
public class Highlighter : MonoBehaviour
{
    private Player owningPlayer;
    private bool playerSelected = false;
    private bool playerTouching = false;
    private bool playerIsHovering = false;

    private MeshRenderer cachedMeshRenderer;

    [SerializeField]
    private float highlightScale = 1f;

    private void Start()
    {
        owningPlayer = GetComponent<Player>();
        Events.instance.AddListener<OnPlayerSelectedEvent>(OnPlayerSelected);
        Events.instance.AddListener<PlayerIsTouchingEvent>(OnCollisionStateChange);
        Events.instance.AddListener<OnSelectorHoverEvent>(OnSelectorHover);
        cachedMeshRenderer = GetComponent<MeshRenderer>();
    }

    private void Update()
    {
        if (playerSelected || (playerIsHovering && playerTouching))
        {
            SetHighlight(1f);
        }
        else if (playerTouching)
        {
            SetHighlight(0.25f);
        }
        else
        {
            SetHighlight(0f);
        }
    }

    private void SetHighlight(float magnitude)
    {
        foreach (Material material in cachedMeshRenderer.materials)
        {
            cachedMeshRenderer.material.SetFloat("_OutlineScalar", magnitude * highlightScale);
        }
    }

    /// <summary>
    /// Called when the player owning this highlighter is inhabited by the player
    /// </summary>
    /// <param name="e"></param>
    private void OnPlayerSelected(OnPlayerSelectedEvent e)
    {
        playerSelected = e.selectedPlayer == owningPlayer;
    }

    /// <summary>
    /// Called when the player is colliding with other inhabitable objects
    /// </summary>
    /// <param name="e"></param>
    private void OnCollisionStateChange(PlayerIsTouchingEvent e)
    {
        playerTouching = e.collisionPlayers.Contains(owningPlayer);

    }

    /// <summary>
    /// Called when the player is hovering over an object in selection mode
    /// </summary>
    /// <param name="e"></param>
    private void OnSelectorHover(OnSelectorHoverEvent e)
    {
        playerIsHovering = e.hoveredPlayer == owningPlayer;
    }

}
