﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIController : MonoBehaviour
{
    [SerializeField]
    private GameObject objectSelectionUI;

    // Start is called before the first frame update
    void Start()
    {
        Events.instance.AddListener<OnCameraControllerStateChange>(OnUIStateChangeMessage);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnUIStateChangeMessage(OnCameraControllerStateChange stateChange)
    {
        if (stateChange.newState == CameraController.States.REGULAR_MOVEMENT)
        {
            objectSelectionUI.SetActive(false);
        }else if (stateChange.newState == CameraController.States.SELECTION_MOVEMENT)
        {
            objectSelectionUI.SetActive(true);
        }
    }
}
