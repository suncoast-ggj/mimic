Alien House Guest
==================
Sound Effects & Events
- GROC an object (select & fully analyze)
- Move an Object
- Highlight a slectable object
- Alarm Warning (you are about to be discovered)
- Alarm (you were discovered & failed this level)
- Received a message from the Commanders
- Display the Codex
- Stow the Codex
- Page Up/Down on the Codex
- Reached the top/bottom of the Codex pages
- My object is blocked from movement (ouch, can't move)
- You have leveled-Up
- You Win (this level)
- Object Thump
- Object Thud
- Object Crash
- Object slides


Music
- Overview / Setup
- Arrival
- Moving carefully
- Searching
- I am in trouble
- I am getting better
- Victory (I won this level)


