﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ABXY.Stately;

[RequireComponent(typeof(Rigidbody), typeof(BoxCollider))]
public class StatelyExample : MonoBehaviour {

    enum CharacterStates { Moving , Jumping}
    StateMachine characterStateMachine = new StateMachine(typeof(CharacterStates));

    void Start()
    {
        characterStateMachine.AddHandlers(
            OnMovingActivated,
            WhileMovingActivated,
            OnMovingDeactivated,
            CharacterStates.Moving);
        characterStateMachine.AddHandlers(
            OnJumpingActivated,
            WhileJumpingActivated,
            OnJumpingDeactivated,
            CharacterStates.Jumping);

        characterStateMachine.SetDefaultState(CharacterStates.Moving);

        characterStateMachine.Start(this);
    }
	
	void Update () {
		characterStateMachine.Update();
	}

    #region Moving State
    private void OnMovingActivated()
    {
        Debug.Log("Character is now in the Moving state");
        //Play moving animation
    }

    private void WhileMovingActivated()
    {
        // Get keyboard logic, move RigidBody

        // If the space bar has been pressed, activate jumping state
        if (Input.GetKeyDown(KeyCode.Space))
            characterStateMachine.ActivateState(CharacterStates.Jumping);
    }

    private void OnMovingDeactivated()
    {
        Debug.Log("Character is no longer in the Moving state");
    }
    #endregion

    #region Jumping state
    private void OnJumpingActivated()
    {
        // Add a vertical force on the character to make them jump
    }

    private void WhileJumpingActivated()
    {
        //if (// Character is touching the ground)
            //characterStateMachine.ActivateState(CharacterStates.Moving);
    }

    private void OnJumpingDeactivated()
    {
        Debug.Log("Character has landed!");
    }
    #endregion

}
