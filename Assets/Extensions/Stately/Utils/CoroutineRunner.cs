﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace ABXY.Stately.Utils
{
    /// <summary>
    /// Utility class allowing any class, even those not derived from Monobehaviour, to 
    /// use coroutines. It also provides methods for running callbacks once coroutines finish execution
    /// </summary>
    public class CoroutineRunner
    {


        /// <summary>
        /// List of groups of coroutines that have callbacks attached to them.
        /// </summary>
        private Dictionary<System.Guid, CallbackContainer> trackedCallbackGroups = new Dictionary<System.Guid, CallbackContainer>();


        /// <summary>
        /// The target object that the coroutine runner uses to create and destroy coroutines
        /// </summary>
        private MonoBehaviour runnerTarget;
        

        /// <summary>
        /// Default constructor. When constructed in this manner, the Coroutine Runner will spawn
        /// a dummy object in the scene to run coroutines on. This is useful if you want to call coroutines from
        /// classes that don't extend Monobeaviour. This method MUST be called from start.
        /// </summary>
        public CoroutineRunner()
        {
            GameObject go = new GameObject();
            go.name = "Coroutine Runner";
            CoroutineTarget target = go.AddComponent<CoroutineTarget>();
            runnerTarget = target;
        }


        /// <summary>
        /// Creates a Coroutine Runner wihout spawining a dummy object in the scene. Instead, a monobehaviour is supplied
        /// in the "owner" paremeter.
        /// </summary>
        /// <param name="owner">The Monobehaviour used to create and destroy coroutines. Although this can be any monobehaviour
        /// it is best to use the calling monobehaviour. That way if the monobehaviour is destroyed the coroutines created by it 
        /// are too.
        /// </param>
        public CoroutineRunner(MonoBehaviour owner)
        {
            runnerTarget = owner;
        }


        /// <summary>
        /// Starts the given coroutine. The onFinish callback is called once the coroutine completes execution
        /// </summary>
        /// <param name="routine">The coroutine</param>
        /// <param name="onFinish"The callback to execute when routine finishes></param>
        public void StartCoroutine(IEnumerator routine, UnityEngine.Events.UnityAction onFinish)
        {
            IEnumerator[] coroutine = new IEnumerator[] { routine };
            StartCoroutines(new List<IEnumerator>(coroutine), onFinish);

        }
        

        /// <summary>
        /// Starts the given coroutine. The onFinish callback is called once the coroutine completes execution
        /// </summary>
        /// <param name="routine">The coroutine</param>
        /// <param name="onFinish"> The callback to execute when routine finishes></param>
        public void StartCoroutine(IEnumerable routine, UnityEngine.Events.UnityAction onFinish)
        {
            StartCoroutine(routine.GetEnumerator(), onFinish);

        }


        /// <summary>
        /// Starts the given coroutines. The onfinish callback is called once ALL coroutines finish executing
        /// </summary>
        /// <param name="onFinish">The callback to execute once all coroutines finish</param>
        /// <param name="coroutines">The coroutines to start</param>
        public void StartCoroutines(UnityEngine.Events.UnityAction onFinish, params IEnumerable[] coroutines)
        {
            StartCoroutines(new List<IEnumerable>(coroutines), onFinish);
        }


        /// <summary>
        /// Starts the given coroutines. The onfinish callback is called once ALL coroutines finish executing
        /// </summary>
        /// <param name="onFinish">The callback to execute once all coroutines finish</param>
        /// <param name="coroutines">The coroutines to start</param>
        public void StartCoroutines(UnityEngine.Events.UnityAction onFinish, params IEnumerator[] coroutines)
        {
            StartCoroutines(new List<IEnumerator>(coroutines), onFinish);
        }


        /// <summary>
        /// Starts the given coroutines. The onfinish callback is called once ALL coroutines finish executing
        /// </summary>
        /// <param name="coroutines">The coroutines to start</param>
        /// <param name="onFinish">The callback to execute once all coroutines finish</param>
        public void StartCoroutines(List<IEnumerable> coroutines, UnityEngine.Events.UnityAction onFinish)
        {
            // Converting all ienumerables to ienumerators
            List<IEnumerator> ienumeratorList = new List<IEnumerator>();
            foreach (IEnumerable coroutine in coroutines)
            {
                ienumeratorList.Add(coroutine.GetEnumerator());
            }

            // starting the coroutines
            StartCoroutines(ienumeratorList, onFinish);
        }

        /// <summary>
        /// Starts the given coroutines. The onfinish callback is called once ALL coroutines finish executing
        /// </summary>
        /// <param name="coroutines">The coroutines to start</param>
        /// <param name="onFinish">The callback to execute once all coroutines finish</param>
        public void StartCoroutines(List<IEnumerator> coroutines, UnityEngine.Events.UnityAction onFinish)
        {

            //Making sure there's no nulls
            for (int index = 0; index < coroutines.Count; index++)
            {
                if (coroutines[index] == null)
                {
                    coroutines.RemoveAt(index);
                }
            }

            // Making and adding my coroutine group
            System.Guid guid = System.Guid.NewGuid();
            trackedCallbackGroups.Add(guid, new CallbackContainer(coroutines, onFinish));

            // starting all of the coroutines in the group;
            foreach (IEnumerator coroutine in coroutines)
            {
                runnerTarget.StartCoroutine(TrackedRoutineRun(coroutine, guid));

            }
        }

        /// <summary>
        /// Coroutine used to run other coroutines in a tracked manner
        /// </summary>
        /// <param name="routine"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        private IEnumerator TrackedRoutineRun(IEnumerator routine, System.Guid id)
        {
            // Starting the coroutine
            yield return runnerTarget.StartCoroutine(routine);

            // The coroutine is done! Time to run the callback and remove the group
            if (trackedCallbackGroups.ContainsKey(id))
            {
                CallbackContainer container = trackedCallbackGroups[id];
                if (container != null)
                {
                    if (container.CallbackFinised())
                    {
                        container.Finish();
                        trackedCallbackGroups.Remove(id);
                    }
                }
            }
        }


        /// <summary>
        /// Stops the given coroutine. Note that stopping a coroutine added via StartCoroutines() will also 
        /// stop all other coroutines started with the same call
        /// </summary>
        /// <param name="routine">The routine to stop</param>
        public void StopCoroutine(IEnumerator routine)
        {
            // Stoping the given coroutine
            runnerTarget.StopCoroutine(routine);

            List<System.Guid> keysToDelete = new List<System.Guid>();

            // Finding the coroutine and ending it
            foreach(KeyValuePair<System.Guid, CallbackContainer> callbackKeyValue in trackedCallbackGroups)
            {
                CallbackContainer container = callbackKeyValue.Value;
                if (container.HasCoroutine(routine)){
                    container.StopCoroutine(routine, runnerTarget);
                    if (container.CallbackFinised())
                    {
                        keysToDelete.Add(callbackKeyValue.Key);
                        
                    }

                }
            }

            // deleting the tracked group the coroutine belongs to.
            foreach(System.Guid guid in keysToDelete)
            {
                CallbackContainer container = trackedCallbackGroups[guid];
                trackedCallbackGroups.Remove(guid);
                container.Finish();
            }
            
        }

        /// <summary>
        /// Stops the given coroutine
        /// </summary>
        /// <param name="routine">The routine to stop</param>
        public void StopCoroutine(IEnumerable routine)
        {
            StopCoroutine(routine.GetEnumerator());
        }

        /// <summary>
        /// Given the ToString() output from a coroutine, this method returns the coroutines original method name
        /// </summary>
        /// <param name="ienumeratorString">THe ToString() output from a coroutine</param>
        /// <returns>The name of the method</returns>
        private string ExtractMethodName(string ienumeratorString)
        {
            string[] splitString = ienumeratorString.Split(new char[] { '<', '>' }, System.StringSplitOptions.RemoveEmptyEntries);
            return splitString[1];
        }

        /// <summary>
        /// Stops all coroutines on this instance of CoroutineRunner
        /// </summary>
        public void StopAllCoroutines()
        {
            runnerTarget.StopAllCoroutines();
            foreach (KeyValuePair<System.Guid, CallbackContainer> callbackKeyValue in trackedCallbackGroups)
            {
                CallbackContainer container = callbackKeyValue.Value;
                container.Finish();
            }
            trackedCallbackGroups.Clear();
            
        }


        /// <summary>
        /// Data container for a collection of ienumerators and a callback
        /// </summary>
        private class CallbackContainer
        {
            
            /// <summary>
            /// The callback to run once all coroutines finish
            /// </summary>
            UnityEngine.Events.UnityAction onFinish;


            /// <summary>
            /// The coroutines associated with the given callback
            /// </summary>
            List<IEnumerator> iEnumerators = new List<IEnumerator>();
            

            /// <summary>
            /// The number of coroutines still running
            /// </summary>
            int numberOfRunningCoroutines = -1;


            /// <summary>
            /// The number of coroutines attached to this callback
            /// </summary>
            public int count
            {
                get
                {
                    return iEnumerators.Count;
                }
            }


            /// <summary>
            /// Default constructor
            /// </summary>
            /// <param name="coroutines">A list of coroutines associated with the onFinish callback</param>
            /// <param name="onFinish">This callback will be executed once ALL coroutines finish executing</param>
            public CallbackContainer( List<IEnumerator> coroutines, UnityEngine.Events.UnityAction onFinish)
            {
                this.iEnumerators = coroutines;
                this.onFinish = onFinish;
                this.numberOfRunningCoroutines = coroutines.Count;
            }


            /// <summary>
            /// Call this method once a coroutine has finished running.
            /// </summary>
            /// <returns>Returns true if all coroutines have finished running, otherwise returns fales</returns>
            public bool CallbackFinised()
            {
                numberOfRunningCoroutines--;
                if (numberOfRunningCoroutines <= 0)
                {
                    return true;
                }
                return false;
            }


            /// <summary>
            /// Triggers a call to this Callback Container's onFinish callback
            /// </summary>
            public void Finish()
            {
                if (onFinish != null)
                    onFinish();
            }


            /// <summary>
            /// Checks if the given coroutine is being managed by this Callback Container
            /// </summary>
            /// <param name="coroutine">The coroutine to check</param>
            /// <returns>True if the coroutine exists, false if not</returns>
            public bool HasCoroutine(IEnumerator coroutine)
            {
                int index = GetCoroutineIndex(coroutine);
                return index != -1;
            }


            /// <summary>
            /// Finds the index in the coroutine list of the given coroutine
            /// </summary>
            /// <param name="coroutine">The coroutine to check</param>
            /// <returns>The index of the coroutine, or -1 if not found</returns>
            private int GetCoroutineIndex(IEnumerator coroutine)
            {
                for (int index = 0; index < iEnumerators.Count; index++)
                {
                    IEnumerator ienumerator = iEnumerators[index];
                    if (ienumerator.ToString() == coroutine.ToString())
                    {
                        return index;
                    }
                }
                return -1;
            }


            /// <summary>
            /// Stops the given coroutine
            /// </summary>
            /// <param name="coroutine">The coroutine to stop</param>
            /// <param name="context">The monobehaviour the coroutine is running on</param>
            public void StopCoroutine(IEnumerator coroutine, MonoBehaviour context)
            {
                // Making sure the coroutine exists
                int index = GetCoroutineIndex(coroutine);

                // Stoping the coroutine
                if (index >= 0)
                {
                    context.StopCoroutine(iEnumerators[index]);
                    this.iEnumerators.RemoveAt(index);
                }
                
            }

        }


    }
}