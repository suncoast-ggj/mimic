﻿using UnityEngine;
using System.Collections;

namespace ABXY.Stately.Utils
{

    /// <summary>
    /// Utility class allowing any class, even those not derived from Monobehaviour, to use
    /// the Update loop. 
    /// </summary>
    public class UpdateRunner : MonoBehaviour
    {

        /// <summary>
        /// Instance of the runner object in the scene
        /// </summary>
        private static UpdateRunner runner;


        /// <summary>
        /// Delegate object for all of the added Update callbacks
        /// </summary>
        private static UnityEngine.Events.UnityAction updateMethods;


        /// <summary>
        /// Adds an update callback to the system.
        /// </summary>
        /// <param name="update">The added callback. This callback will be invoked every frame</param>
        public static void AddUpdateCallback(UnityEngine.Events.UnityAction update)
        {
            // Building a runner object if one does not exist
            if (runner == null)
            {
                GameObject go = new GameObject();
                go.name = "Temporary - Update Runner";
                UpdateRunner.runner = go.AddComponent<UpdateRunner>();
            }

            // Adding the update method
            updateMethods -= update;
            updateMethods += update;
        }


        /// <summary>
        /// Calling the update callbacks
        /// </summary>
        public void Update()
        {
            if (updateMethods != null)
                updateMethods();
            else
                Destroy(this.gameObject);
        }

    }
}