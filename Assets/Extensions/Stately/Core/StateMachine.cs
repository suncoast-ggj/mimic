﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Reflection;
using ABXY.Stately.Utils;

namespace ABXY.Stately{

    /// <summary>
    /// Simple state machine system - documentation to come
    /// </summary>
    [System.Serializable]
    public class StateMachine : ISerializationCallbackReceiver
    {

        /// <summary> List of states in this object</summary>
        [SerializeField]
        private List<State> states = new List<State>(0);


        /// <summary>The current state of the state machine</summary>
        [SerializeField, HideInInspector]
        private State currentState_P;

        /// <summary>
        /// The next state - used in state switching
        /// </summary>
        private State nextState;

        /// <summary>
        /// Is the state machine expanded in the inspector?
        /// </summary>
        [SerializeField, HideInInspector]
        private bool expanded = false;

        /// <summary>
        /// The type of the enum definition for this state machine
        /// </summary>
        private Type enumType;

        /// <summary>
        /// The index of the state the user has chosen to be the initial state
        /// </summary>
        [SerializeField]
        private int initialStateIndex = 0;

        /// <summary>
        /// Coroutine runner - used to run managed coroutines
        /// </summary>
        private CoroutineRunner coroutineRunner;

        //TODO remove, this is stupid
        private UnityEngine.Events.UnityAction fromAnyState;

        /// <summary>Returns the name of the current state of the state machine. Not assignable</summary>
        public string currentStateName
        {
            get
            {
                if (currentState_P == null && states.Count > 0)
                    currentState_P = states[0];
                return currentState_P.stateName;
            }
        }

        /// <summary>Returns the current state of the state machine. Not assignable</summary>
        private object currentState
        {
            get
            {
                if (currentState_P == null && states.Count > 0)
                    currentState_P = states[0];
                return System.Enum.Parse(enumType, currentState_P.stateName);
            }
        }

        /// <summary>
        /// Constructor. Generates a state machine with the given enum state definition
        /// </summary>
        /// <param name="enumType"></param>
        public StateMachine(Type enumType)
        {
            this.enumType = enumType;
            // Setting the initial state
            OnAfterDeserialize();
        }


        /// <summary>
        /// Starts the state machine
        /// </summary>
        private void FinishStart()
        {
            if (coroutineRunner == null)
            {
                coroutineRunner = new CoroutineRunner();
            }
            State initialState = states[initialStateIndex];
            if (initialState != currentState_P)
            {
                currentState_P = initialState;
                ActivateState(initialState);
            }
        }


        /// <summary>
        /// Starts the state machine
        /// </summary>
        /// <param name="owner">The gameobject that owns the state machine</param>
        public void Start(MonoBehaviour owner)
        {
            if (owner == null)
                Debug.LogError("'owner' cannot be null");
            else
            {
                coroutineRunner = new CoroutineRunner(owner);
                FinishStart();
            }
        }

        /// <summary>
        /// Starts the state machine
        /// </summary>
        public void Start()
        {
            coroutineRunner = new CoroutineRunner();
            FinishStart();
        }

        /// <summary>
        /// Sets the first state run in the state machine
        /// </summary>
        /// <param name="stateEnum"></param>
        public void SetDefaultState(object stateEnum)
        {
            initialStateIndex = (int)stateEnum;
            //State initialState = states[initialStateIndex];
            //ActivateState(initialState);

        }


        /// <summary>
        /// Sets the state machine to automatically update, as an alternative to calling Update() every frame. Note that this
        /// spawns an extra object into the scene, introducing some pe  1rformance overhead
        /// </summary>
        public void SetAutoUpdate()
        {
            UpdateRunner.AddUpdateCallback(Update);

        }

        /// <summary>
        /// Activates the given state name, assuming it is not disabled
        /// </summary>
        /// <param name="stateEnum">The eSnum of the state. Enum must match the enum definition</param>
        public void ActivateState(object stateEnum)
        {
            State givenState = GetStateByEnum(stateEnum);
            if (givenState != null && givenState.enabled)
            {
                ActivateState(givenState);
            }

        }


        /// <summary>
        /// Sets the given state as enabled or disabled. Disabling a state prevents
        /// the state machine from switching to that state
        /// </summary>
        /// <param name="stateEnum">The enum of the state. Enum must match the enum definition</param>
        /// <param name="enabled">Enable or disable the state</param>
        private void SetStateEnabled(object stateEnum, bool enabled)
        {
            State givenState = GetStateByEnum(stateEnum);
            if (givenState != null)
            {
                givenState.enabled = enabled;
            }
        }


        /// <summary>
        /// Searches through the state list for the given state. Returns it
        /// if found, otherwise returns null
        /// </summary>
        /// <param name="stateEnum">The enum of the state to search for. Enum must match the enum definition</param>
        /// <returns>The state if found, or null</returns>
        public State GetStateByEnum(object stateEnum)
        {
            State returnedState = null;
            foreach (State state in states)
            {
                if (state.enumIndex.Equals((int)stateEnum))
                {
                    returnedState = state;
                }
            }
            return returnedState;
        }

        /// <summary>
        /// Add activation, deactivation and while activation handelers in bulk. Handlers may be null
        /// </summary>
        /// <param name="onActivate">This handler will be called once when this state is activated</param>
        /// <param name="whileActivated">This handler will be called continuously while this state is activated</param>
        /// <param name="onDeactivated">This handler will be called once when this state is deactivated</param>
        /// <param name="stateEnum">The state these handlers are attached to</param>
        public void AddHandlers(UnityEngine.Events.UnityAction onActivate, 
            UnityEngine.Events.UnityAction whileActivated,
            UnityEngine.Events.UnityAction onDeactivated,
            object stateEnum)
        {
            if (onActivate != null)
                AddOnActivateHandler(onActivate, stateEnum);
            if (whileActivated != null)
                AddWhileActivatedHandler(whileActivated, stateEnum);
            if (onActivate != null)
                AddOnDeactivateHandler(onDeactivated, stateEnum);
        }

        /// <summary>
        /// Add activation, deactivation and while activation handelers in bulk. Handlers may be null
        /// </summary>
        /// <param name="onActivate">This handler will be called once when this state is activated</param>
        /// <param name="whileActivated">This handler will be called continuously while this state is activated</param>
        /// <param name="onDeactivated">This handler will be called once when this state is deactivated</param>
        /// <param name="stateEnum">The state these handlers are attached to</param>
        public void AddHandlers(IEnumerable onActivate,
            UnityEngine.Events.UnityAction whileActivated,
            UnityEngine.Events.UnityAction onDeactivated,
            object stateEnum)
        {
            if (onActivate != null)
                AddOnActivateHandler(onActivate, stateEnum);
            if (whileActivated != null)
                AddWhileActivatedHandler(whileActivated, stateEnum);
            if (onActivate != null)
                AddOnDeactivateHandler(onDeactivated, stateEnum);
        }

        /// <summary>
        /// Add activation, deactivation and while activation handelers in bulk. Handlers may be null
        /// </summary>
        /// <param name="onActivate">This handler will be called once when this state is activated</param>
        /// <param name="whileActivated">This handler will be called continuously while this state is activated</param>
        /// <param name="onDeactivated">This handler will be called once when this state is deactivated</param>
        /// <param name="stateEnum">The state these handlers are attached to</param>
        public void AddHandlers(IEnumerable onActivate,
            IEnumerable whileActivated,
            IEnumerable onDeactivated,
            object stateEnum)
        {
            if (onActivate != null)
                AddOnActivateHandler(onActivate, stateEnum);
            if (whileActivated != null)
                AddWhileActivatedHandler(whileActivated, stateEnum);
            if (onActivate != null)
                AddOnDeactivateHandler(onDeactivated, stateEnum);
        }


        /// <summary>
        /// Add activation, deactivation and while activation handelers in bulk. Handlers may be null
        /// </summary>
        /// <param name="onActivate">This handler will be called once when this state is activated</param>
        /// <param name="whileActivated">This handler will be called continuously while this state is activated</param>
        /// <param name="onDeactivated">This handler will be called once when this state is deactivated</param>
        /// <param name="stateEnum">The state these handlers are attached to</param>
        public void AddHandlers(UnityEngine.Events.UnityAction onActivate,
            IEnumerable whileActivated,
            UnityEngine.Events.UnityAction onDeactivated,
            object stateEnum)
        {
            if (onActivate != null)
                AddOnActivateHandler(onActivate, stateEnum);
            if (whileActivated != null)
                AddWhileActivatedHandler(whileActivated, stateEnum);
            if (onActivate != null)
                AddOnDeactivateHandler(onDeactivated, stateEnum);
        }


        /// <summary>
        /// Add activation, deactivation and while activation handelers in bulk. Handlers may be null
        /// </summary>
        /// <param name="onActivate">This handler will be called once when this state is activated</param>
        /// <param name="whileActivated">This handler will be called continuously while this state is activated</param>
        /// <param name="onDeactivated">This handler will be called once when this state is deactivated</param>
        /// <param name="stateEnum">The state these handlers are attached to</param>
        public void AddHandlers(UnityEngine.Events.UnityAction onActivate,
            UnityEngine.Events.UnityAction whileActivated,
            IEnumerable onDeactivated,
            object stateEnum)
        {
            if (onActivate != null)
                AddOnActivateHandler(onActivate, stateEnum);
            if (whileActivated != null)
                AddWhileActivatedHandler(whileActivated, stateEnum);
            if (onActivate != null)
                AddOnDeactivateHandler(onDeactivated, stateEnum);
        }


        /// <summary>
        /// Add activation, deactivation and while activation handelers in bulk. Handlers may be null
        /// </summary>
        /// <param name="onActivate">This handler will be called once when this state is activated</param>
        /// <param name="whileActivated">This handler will be called continuously while this state is activated</param>
        /// <param name="onDeactivated">This handler will be called once when this state is deactivated</param>
        /// <param name="stateEnum">The state these handlers are attached to</param>
        public void AddHandlers(IEnumerable onActivate,
            IEnumerable whileActivated,
            UnityEngine.Events.UnityAction onDeactivated,
            object stateEnum)
        {
            if (onActivate != null)
                AddOnActivateHandler(onActivate, stateEnum);
            if (whileActivated != null)
                AddWhileActivatedHandler(whileActivated, stateEnum);
            if (onActivate != null)
                AddOnDeactivateHandler(onDeactivated, stateEnum);
        }


        /// <summary>
        /// Add activation, deactivation and while activation handelers in bulk. Handlers may be null
        /// </summary>
        /// <param name="onActivate">This handler will be called once when this state is activated</param>
        /// <param name="whileActivated">This handler will be called continuously while this state is activated</param>
        /// <param name="onDeactivated">This handler will be called once when this state is deactivated</param>
        /// <param name="stateEnum">The state these handlers are attached to</param>
        public void AddHandlers(UnityEngine.Events.UnityAction onActivate,
            IEnumerable whileActivated,
            IEnumerable onDeactivated,
            object stateEnum)
        {
            if (onActivate != null)
                AddOnActivateHandler(onActivate, stateEnum);
            if (whileActivated != null)
                AddWhileActivatedHandler(whileActivated, stateEnum);
            if (onActivate != null)
                AddOnDeactivateHandler(onDeactivated, stateEnum);
        }


        /// <summary>
        /// Add activation, deactivation and while activation handelers in bulk. Handlers may be null
        /// </summary>
        /// <param name="onActivate">This handler will be called once when this state is activated</param>
        /// <param name="whileActivated">This handler will be called continuously while this state is activated</param>
        /// <param name="onDeactivated">This handler will be called once when this state is deactivated</param>
        /// <param name="stateEnum">The state these handlers are attached to</param>
        public void AddHandlers(IEnumerable onActivate,
            UnityEngine.Events.UnityAction whileActivated,
            IEnumerable onDeactivated,
            object stateEnum)
        {
            if (onActivate != null)
                AddOnActivateHandler(onActivate, stateEnum);
            if (whileActivated != null)
                AddWhileActivatedHandler(whileActivated, stateEnum);
            if (onActivate != null)
                AddOnDeactivateHandler(onDeactivated, stateEnum);
        }


        /// <summary>
        /// Add an activate, deactivate, or while state handler
        /// </summary>
        /// <param name="handlerType">Is this an activate, deactivate, or while handler?</param>
        /// <param name="handler">The handler</param>
        /// <param name="stateEnum">The state this handler belongs to</param>
        public void AddHandler(StatelyEnums.HandlerType handlerType, UnityEngine.Events.UnityAction handler, object stateEnum)
        {
            if (handlerType == StatelyEnums.HandlerType.OnActivate)
            {
                AddOnActivateHandler(handler, stateEnum);
            }else if (handlerType == StatelyEnums.HandlerType.OnDeactivate)
            {
                AddOnDeactivateHandler(handler, stateEnum);
            }else if (handlerType == StatelyEnums.HandlerType.WhileActivated)
            {
                AddWhileActivatedHandler(handler, stateEnum);
            }
        }


        /// <summary>
        /// Add an activate, deactivate, or while state handler
        /// </summary>
        /// <param name="handlerType">Is this an activate, deactivate, or while handler?</param>
        /// <param name="handler">The handler</param>
        /// <param name="stateEnum">The state this handler belongs to</param>
        public void AddHandler(StatelyEnums.HandlerType handlerType, IEnumerable handler, object stateEnum)
        {
            if (handlerType == StatelyEnums.HandlerType.OnActivate)
            {
                AddOnActivateHandler(handler, stateEnum);
            }
            else if (handlerType == StatelyEnums.HandlerType.OnDeactivate)
            {
                AddOnDeactivateHandler(handler, stateEnum);
            }
            else if (handlerType == StatelyEnums.HandlerType.WhileActivated)
            {
                AddWhileActivatedHandler(handler, stateEnum);
            }
        }

        /// <summary>
        /// Adds an onactivate handler to the given state
        /// </summary>
        /// <param name="onActivate">The delegate to call when the state is activated</param>
        /// <param name="stateEnum">The enum of the state. Enum must match the enum definition</param>
        private void AddOnActivateHandler(UnityEngine.Events.UnityAction onActivate, object stateEnum)
        {
            State state = GetStateByEnum(stateEnum);
            if (state != null)
            {
                state.OnActivate.AddListener(onActivate);
            }
        }


        /// <summary>
        /// Adds an onactivate handler to the given state
        /// </summary>
        /// <param name="onActivate">The coroutine to call when the state is activated</param>
        /// <param name="stateEnum">The enum of the state. Enum must match the enum definition</param>
        private void AddOnActivateHandler(IEnumerable onActivate, object stateEnum)
        {
            State state = GetStateByEnum(stateEnum);
            if (state != null)
            {
                state.onActivateCoroutines.Add(onActivate);
            }
        }

        //TODO remove?
        public void AddFromAnyStateHandler(UnityEngine.Events.UnityAction fromAnyState)
        {
            this.fromAnyState += fromAnyState;
        }


        /// <summary>
        /// Adds an onDeactivate handler to the given state
        /// </summary>
        /// <param name="onDeactivate">The delegate to call when the state is deactivated</param>
        /// <param name="stateEnum">The enum of the state. Enum must match the enum definition</param>
        private void AddOnDeactivateHandler(UnityEngine.Events.UnityAction onDeactivate, object stateEnum)
        {
            State state = GetStateByEnum(stateEnum);
            if (state != null)
            {
                state.OnDeactivate.AddListener(onDeactivate);
            }
        }

        /// <summary>
        /// Adds an onDeactivate handler to the given state
        /// </summary>
        /// <param name="onDeactivate">The coroutine to call when the state is deactivated</param>
        /// <param name="stateEnum">The enum of the state. Enum must match the enum definition</param>
        private void AddOnDeactivateHandler(IEnumerable onDeactivate, object stateEnum)
        {
            State state = GetStateByEnum(stateEnum);
            if (state != null)
            {
                state.onDeactivateCoroutines.Add(onDeactivate);
            }
        }

        /// <summary>
        /// Adds a whileActivated handler to the given state
        /// </summary>
        /// <param name="whileActivated">The coroutine to call while the state is activated</param>
        /// <param name="stateEnum">The enum of the state. Enum must match the enum definition</param>
        private void AddWhileActivatedHandler(IEnumerable whileActivated, object stateEnum)
        {
            State state = GetStateByEnum(stateEnum);
            if (state != null && !state.whileActivatedCoroutines.Contains(whileActivated))
            {
                state.whileActivatedCoroutines.Add(whileActivated);
            }
        }


        /// <summary>
        /// Adds a whileActivated handler to the given state
        /// </summary>
        /// <param name="whileActivated">The delegate to call while the state is activated</param>
        /// <param name="stateEnum">The enum of the state. Enum must match the enum definition</param>
        private void AddWhileActivatedHandler(UnityEngine.Events.UnityAction whileActivated, object stateEnum)
        {
            State state = GetStateByEnum(stateEnum);
            if (state != null)
            {
                state.WhileActivated.AddListener(whileActivated);
            }
        }

        /// <summary>
        /// Activates a state, sets it as the new current. Calls proper activation functions
        /// </summary>
        /// <param name="newCurrentState">The new current state</param>
        private void ActivateState(State newCurrentState)
        {

            //stopping all coroutines
            if (currentState_P.whileActivatedCoroutines.Count > 0)
            {
                foreach (IEnumerable coroutine in currentState_P.whileActivatedCoroutines)
                {
                    coroutineRunner.StopCoroutine(coroutine);
                }

            }

            if (currentState_P.OnDeactivate != null)
                currentState_P.OnDeactivate.Invoke();

            nextState = newCurrentState;

            if (currentState_P.onDeactivateCoroutines.Count > 0)
            {
                // Doing OnDeActivate Coroutines

                coroutineRunner.StartCoroutines(currentState_P.onDeactivateCoroutines, ContinueActivatingState);
            }
            else
            {
                ContinueActivatingState();
            }
        }

        /// <summary>
        /// Method called agter OnActivate coroutines are run
        /// </summary>
        private void ContinueActivatingState()
        {
            // Doing OnActivated functions
            if (nextState.OnActivate != null)
            {
                nextState.OnActivate.Invoke();
            }

            if (nextState.onActivateCoroutines.Count > 0)
            {
                // Doing OnActivate Coroutines
                coroutineRunner.StartCoroutines(nextState.onActivateCoroutines, FinishActivatingState);
            }
            else
            {
                FinishActivatingState();
            }
        }

        /// <summary>
        /// Called after all OnActivate functions are called
        /// </summary>
        private void FinishActivatingState()
        {
            // doing whileActivated Coroutines
            if (nextState.whileActivatedCoroutines.Count > 0)
            {

                coroutineRunner.StartCoroutines(nextState.whileActivatedCoroutines, null);

            }
            currentState_P = nextState;
        }


        /// <summary>
        /// Updates the currently activated state. Must be called every time you want to update a state
        /// i.e., every frame
        /// </summary>
        public void Update()
        {
            if (currentState_P != null)
            {
                currentState_P.WhileActivated.Invoke();
                if (fromAnyState != null)
                    fromAnyState();
            }
        }

        /// <summary>
        /// Do not call - Hook required for serialization, but unused
        /// </summary>
        public void OnBeforeSerialize()
        {

        }

        /// <summary>
        /// Do not call - serialization hook - Used to preserve callback setups through changes in the state machine's enum definition
        /// </summary>
        public void OnAfterDeserialize()
        {
            if (enumType != null)
            {
                //Grabbing the enum values from the enum definition. 
                FieldInfo[] enumValues = enumType.GetFields(BindingFlags.Public | BindingFlags.Static);

                // going to yell at the user if the enum isn't valid.
                if (enumType.IsEnum)
                {
                    /*
                     * Going to rebuild states. This piece of code adds any new states from the enum definition to a new state list,
                     * copies over pre-existing states to the new state list from the old state list, and then swaps the two lists
                     * */

                    List<State> newStates = new List<State>();
                    foreach (FieldInfo value in enumValues)
                    {
                        object state = value.GetValue(null);
                        State oldState = this.GetStateByEnum(state);
                        if (oldState == null)
                        {
                            newStates.Add(new State((int)state, value.Name));
                        }
                        else
                        {
                            newStates.Add(oldState);
                        }


                    }
                    states = newStates;
                    // Making sure the initial state index is still in bounds
                    if (initialStateIndex >= states.Count || initialStateIndex < 0)
                    {
                        initialStateIndex = states.Count - 1;
                    }


                }
                else
                {
                    Debug.LogError("ERROR - Supplied state machine states must be an enum type");
                }
            }
            else
            {
                Debug.LogError("Do not initialize a state machine from inside a method. The state machine will not function properly");
            }
        }
        
        /// <summary>
        /// Checks if the currently running state matches the given state
        /// </summary>
        /// <param name="state">The state to check</param>
        /// <returns>Is the given state currently running?</returns>
        public bool CurrentStateIs(object state)
        {
            return currentState.Equals(state);

        }

        /// <summary>
        /// Returns the number of states in this state machine
        /// </summary>
        /// <returns>The number of states in this state machine</returns>
        public int StateCount()
        {
            return states.Count;
        }
    }
}