﻿using UnityEngine;
using UnityEditor;
using ABXY.Stately;

namespace ABXY.Stately.Editor
{
    /// <summary>
    /// Custom property drawer for displaying State Machines
    /// </summary>
    [CustomPropertyDrawer(typeof(StateMachine))]
    public class IngredientDrawer : PropertyDrawer
    {
        /// <summary>
        /// Drawing the state machine
        /// </summary>
        /// <param name="position"></param>
        /// <param name="property"></param>
        /// <param name="label"></param>
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);
            SerializedProperty expandedProperty = property.FindPropertyRelative("expanded");


            // Going to draw the title. Inserting the current state into the title if the game is running
            string statusText = "";
            SerializedProperty currentState = property.FindPropertyRelative("currentState_P");
            if (Application.isPlaying && currentState != null)
            {
                statusText = "Current state: " + currentState.FindPropertyRelative("stateName").stringValue;
            }
            expandedProperty.boolValue = EditorGUI.Foldout(new Rect(position.x, position.y, position.width, 25f),expandedProperty.boolValue ,new GUIContent( label.text + "      " + statusText));

            position.y += 15f;

            if (expandedProperty.boolValue)
            {
                // Getting the current state list
                SerializedProperty statesList = property.FindPropertyRelative("states");
                EditorGUI.indentLevel += 1;


                //Making an array of state names to use with the default state dropdown
                string[] stateNames = new string[statesList.arraySize];
                for (int index = 0; index < statesList.arraySize; index++)
                {
                    stateNames[index] = statesList.GetArrayElementAtIndex(index).FindPropertyRelative("stateName").stringValue;

                }

                // Drawing the default state dropdown
                SerializedProperty initialStateIndexProperty = property.FindPropertyRelative("initialStateIndex");
                initialStateIndexProperty.intValue = EditorGUI.Popup(new Rect(position.x, position.y, position.width, 20), "Default State", initialStateIndexProperty.intValue, stateNames);

                position.y += 20f;
                // Drawing each state
                EditorGUI.indentLevel += 1;
                for (int index = 0; index < statesList.arraySize; index++)
                {
                    SerializedProperty currentProperty = statesList.GetArrayElementAtIndex(index);
                    EditorGUI.PropertyField(new Rect(position.x, position.y, 30, position.height), currentProperty, GUIContent.none);
                    position.y += EditorGUI.GetPropertyHeight(currentProperty);
                }
                EditorGUI.indentLevel -= 1;
                EditorGUI.indentLevel -= 1;
                EditorGUI.EndProperty();
            }
        }

        /// <summary>
        /// Calculating the height of this drawer
        /// </summary>
        /// <param name="property"></param>
        /// <param name="label"></param>
        /// <returns></returns>
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            float height = 0f;
            SerializedProperty expandedProperty = property.FindPropertyRelative("expanded");

            // Height of the title bar
            height += 20f;

            if (expandedProperty.boolValue)
            {
                // Height of the default state selector
                height += 15f;

                // Heights of the states
                SerializedProperty statesList = property.FindPropertyRelative("states");
                for (int index = 0; index < statesList.arraySize; index++)
                {
                    SerializedProperty currentProperty = statesList.GetArrayElementAtIndex(index);
                    height += EditorGUI.GetPropertyHeight(currentProperty);
                }
            }
            return height;

        }
    }
}